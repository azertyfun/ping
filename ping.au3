#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include "Includes/opengl.au3"

Func readConfig($key, $default)
	Local $read = IniRead("ping.ini", "config", $key, "none")
	if StringCompare($read, "none") == 0 Then
		IniWrite("ping.ini", "config", $key, $default)
		return $default
	EndIf
	return $read
EndFunc

Func checkCloseNeeded()
	If glfwGetWindowParam($GLFW_OPENED) == true Then
		Exit(0)
	EndIf
EndFunc

; --------------------
; Getting config keys
; --------------------
ConsoleWrite("Reading configuration..." & @CRLF)

$ip = readConfig("ip", "google.com")
$RESULTS_IN_BUFFER = readConfig("bufferLength", 100)
$red = Int(readConfig("red", 255))
$green = Int(readConfig("green", 127))
$blue = Int(readConfig("blue", 36))
$timeout = readConfig("timeout", 2000)
$TIME_BETWEEN_FRAMES = readConfig("timeBetweenFrames", 500)
ConsoleWrite("R=" & $red & ", G=" & $green & ", B=" & $blue & @CRLF)

Local $buffer[$RESULTS_IN_BUFFER]
$j = 0

$number = 0
$total = 0

;Creating window
ConsoleWrite("Initializing OpenGL..." & @CRLF)

glInit()
gluInit()
glfwInit()

glfwOpenWindowHint($GLFW_WINDOW_NO_RESIZE, 1)
glfwOpenWindow(500, 500, 8, 8, 8, 0, 8, 0, $GLFW_WINDOW)
glfwSetWindowTitle("Ping Graphic - Current ping : 0ms - Average : 0ms")
glOrtho(0, 500, 500, 0, 1, -1)

ConsoleWrite("Initialized OpenGL..." & @CRLF)

AdlibRegister("checkCloseNeeded", 100)

While True
	glMatrixMode($GL_PROJECTION)
	glLoadIdentity()
	glClear($GL_COLOR_BUFFER_BIT)
	
	if $j == $RESULTS_IN_BUFFER Then
		For $i = 0 To $RESULTS_IN_BUFFER -1 Step +1
			If $i <> $RESULTS_IN_BUFFER -1 Then
				$buffer[$i] = $buffer[$i + 1]
			EndIf
		Next
		$j = $RESULTS_IN_BUFFER -1
	EndIf
	
	$ping = Ping($ip, $timeout)
	
	If $ping == 0 Then
		$ping = $timeout
	EndIf
	
	$buffer[$j] = $ping
	
	$number = $number + 1
	$total = $total + $ping
	
	$lastX = 0
	$lastY = 0
	
	glBegin($GL_QUADS)
		glColor3f(255, 255, 255)
		glVertex2f(0, 0)
		glVertex2f(0, 500)
		glVertex2f(500, 500)
		glVertex2f(500, 0)
	glEnd()
	
	glColor3ub($red, $green, $blue)
	glfwSetWindowTitle("Ping Graphic - Current : " & $ping & "ms - Average : " & Round($total/$number) & "ms")
	For $i = 0 To $RESULTS_IN_BUFFER -1 Step +1
		;Writing the graphic...
		$x = $i * 500 / $RESULTS_IN_BUFFER
		$y = 500 - $buffer[$i]
		
		glBegin($GL_LINES)
			glVertex2f($lastX, $lastY)
			glVertex2f($x, $y)
		glEnd()
		
		$lastX = $x
		$lastY = $y
	Next
	
	$j = $j + 1
	
	glfwSwapBuffers()
	Sleep($TIME_BETWEEN_FRAMES)
WEnd

glfwTerminate()
glTerminate()
